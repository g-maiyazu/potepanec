class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.all.includes(:root)
    @taxon = Spree::Taxon.find(params[:id])
    taxon_products = @taxon.all_products.includes(master: %i[default_price images])
    @products = sort_products_by(params[:sort_type], taxon_products)
    respond_to do |format|
      format.html
      format.js
    end
  end

  private

  def sort_products_by(sort_type, products)
    return products if sort_type.nil?

    case sort_type.to_i
    when Constants::SortType::POPULARITY
      products
    when Constants::SortType::PRICE_LOW_TO_HIGH
      products.sort_by(&:price)
    when Constants::SortType::NEW_ARRIVAL_ORDER
      products.sort_by(&:created_at).reverse
    when Constants::SortType::PRICE_HIGH_TO_LOW
      products.sort_by(&:price).reverse
    else
      products
    end
  end
end
