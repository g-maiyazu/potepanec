module Constants
  module SortType
    POPULARITY = 1
    PRICE_LOW_TO_HIGH = 2
    NEW_ARRIVAL_ORDER = 3
    PRICE_HIGH_TO_LOW = 4
  end
end
