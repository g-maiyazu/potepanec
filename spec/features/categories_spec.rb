require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  let(:base_title) { 'BIGBAG' }
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let(:other_product) { create(:product) }
  let(:clothing) { taxonomy.root.children.create(name: 'Clothing') }
  let(:shirts) { clothing.children.create(name: 'Shirts', taxonomy: taxonomy) }
  let!(:high_price_new_item) do
    product = create(:product, name: 'high_price_new_item',
                               price: 1000,
                               created_at: 1.day.ago)
    product.taxons << shirts
    product.images.create!(attachment: create(:image).attachment)
    product
  end
  let!(:low_price_old_item) do
    product = create(:product, name: 'low_price_old_item',
                               price: 100,
                               created_at: 10.days.ago)
    product.taxons << shirts
    product.images.create!(attachment: create(:image).attachment)
    product
  end
  let!(:normal_item) do
    product = create(:product, name: 'normal_item',
                               price: 500,
                               created_at: 5.days.ago)
    product.taxons << shirts
    product.images.create!(attachment: create(:image).attachment)
    product
  end

  describe 'カテゴリページの操作' do
    before do
      visit potepan_category_path(taxon.id)
    end

    it 'カテゴリがタイトルに正しく表示されていること' do
      expect(page).to have_title "カテゴリ - #{taxon.name} | #{base_title}"
    end

    it '表示の確認がとれること' do
      expect(page).to have_content(taxonomy.name)
      expect(page).to have_content(product.display_price)
      expect(page).to have_content(product.name)
      expect(page).to have_no_content(other_product.name)
    end

    it '詳細ページへのリンクを持っていること' do
      expect(page).to have_link href: potepan_product_path(product.id)
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end

  describe 'ソート順の確認' do
    it '商品がデフォルト/人気順で並んでいること' do
      visit potepan_category_path(id: shirts.id)
      within 'div#show_products' do
        product_name = page.all('h5')
        expect(product_name[0].text).to eq 'high_price_new_item'
        expect(product_name[1].text).to eq 'low_price_old_item'
        expect(product_name[2].text).to eq 'normal_item'
      end
    end

    it '商品が新着順に並んでいること' do
      expect_sort_to_be_success(Constants::SortType::NEW_ARRIVAL_ORDER)
    end

    it '商品が高い順に並んでいること' do
      expect_sort_to_be_success(Constants::SortType::PRICE_HIGH_TO_LOW)
    end

    context '安い順にソートした場合' do
      # high_price_new_itemの価格を低価格に更新
      let(:high_price_new_item) do
        product = create(:product, name: 'high_price_new_item', price: 100)
        product.taxons << shirts
      end
      # low_price_old_itemの価格を高価格に更新
      let(:low_price_old_item) do
        product = create(:product, name: 'low_price_old_item', price: 1000)
        product.taxons << shirts
      end

      it '商品が安い順に並んでいること' do
        expect_sort_to_be_success(Constants::SortType::PRICE_LOW_TO_HIGH)
      end
    end

    def expect_sort_to_be_success(sort_type)
      visit potepan_category_path(id: shirts.id, sort_type: sort_type)
      within 'div#show_products' do
        product_name = page.all('h5')
        expect(product_name[0].text).to eq 'high_price_new_item'
        expect(product_name[1].text).to eq 'normal_item'
        expect(product_name[2].text).to eq 'low_price_old_item'
      end
    end
  end
end
