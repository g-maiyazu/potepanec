require 'rails_helper'

RSpec.feature 'Products', type: :feature do
  let(:base_title) { 'BIGBAG' }
  let(:product) { create(:product) }

  describe 'GET #show' do
    before do
      visit potepan_product_path(product.id)
    end

    it '商品詳細ページのタイトルが正しく表示されていること' do
      expect(page).to have_title "#{product.name} | #{base_title}"
    end
  end
end
