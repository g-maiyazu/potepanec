require 'rails_helper'

RSpec.describe 'Categories', type: :request do
  describe 'カテゴリページの取得' do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it '正しいレスポンスが確認されること' do
      expect(response).to have_http_status(200)
    end

    it '正しいテンプレートが表示されること' do
      expect(response).to render_template :show
    end

    describe 'パラメータの取得' do
      it '@taxonomiesが期待される値を持つこと' do
        expect(assigns(:taxonomies)).to contain_exactly(taxonomy)
      end

      it '@taxonが期待される値を持つこと' do
        expect(assigns(:taxon)).to eq taxon
      end

      it '@productsが期待される値を持つこと' do
        expect(assigns(:products)).to contain_exactly(product)
      end
    end
  end
end
