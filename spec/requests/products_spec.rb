require 'rails_helper'

RSpec.describe 'ProductsRequests', type: :request do
  describe 'GET #show' do
    let(:product) do
      create(:product, name: 'example_item', price: 123.45, description: 'foobar')
    end

    before do
      get potepan_product_path(product.id)
    end

    it '正しいレスポンスが確認されること' do
      expect(response).to have_http_status(200)
    end

    it '正しいテンプレートが表示されること' do
      expect(response).to render_template :show
    end

    describe 'get product params' do
      it '@productが期待される値を持つこと' do
        expect(assigns(:product)).to eq product
      end

      it '商品名がページ内に表示されること' do
        expect(response.body).to include('example_item')
      end

      it '値段がページ内に表示されること' do
        expect(response.body).to include('123.45')
      end

      it '商品説明がページ内に表示されること' do
        expect(response.body).to include('foobar')
      end
    end
  end
end
